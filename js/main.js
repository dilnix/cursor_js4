const url = 'https://api.monobank.ua/bank/currency';
const curData = document.querySelector('.cur-data');
const results = document.querySelector('.results');
const buy = document.querySelector('.buy');
const sell = document.querySelector('.sell');

let currencies = fetch(url)
    .then(response => response.json())
    .then(data => {
        const usdRateBuy = data[0].rateBuy;
        const usdRateSell = data[0].rateSell;
        const eurRateBuy = data[1].rateBuy;
        const eurRateSell = data[1].rateSell;
        const plnRateBuy = data[4].rateBuy;
        const plnRateSell = data[4].rateSell;

        curData.insertAdjacentHTML('afterbegin', `<p>Buy (Купівля): <b>${usdRateBuy}</b> UAH for 1 USD (Долар США)</p>
        <p>Sell (Продаж): <b>${usdRateSell}</b> UAH for 1 USD (Долар США)</p><hr>
        <p>Buy (Купівля): <b>${eurRateBuy}</b> UAH for 1 EUR (Євро)</p>
        <p>Sell (Продаж): <b>${eurRateSell}</b> UAH for 1 EUR (Євро)</p><hr>
        <p>Buy (Купівля): <b>${plnRateBuy}</b> UAH for 1 PLN (Польський Злотий)</p>
        <p>Sell (Продаж): <b>${plnRateSell}</b> UAH for 1 PLN (Польський Злотий)</p><hr>`);

        buy.onclick = () => {
            const currIndex = document.querySelector('.currency').options.selectedIndex;
            const currency = document.querySelector('.currency').options[currIndex].text;
            const amount = document.querySelector('.amount').value;
            countBuy(currency, amount);
        };

        sell.onclick = () => {
            const currIndex = document.querySelector('.currency').options.selectedIndex;
            const currency = document.querySelector('.currency').options[currIndex].text;
            const amount = document.querySelector('.amount').value;
            countSell(currency, amount);
        };

        countBuy = (currency, amount) => {
            let result;

            switch (currency) {
                case 'USD':
                    result = amount * usdRateBuy;
                    console.log('usdRateBuy: ', usdRateBuy);
                    console.log('amount: ', amount);
                    break;
                case 'EUR':
                    result = amount * eurRateBuy;
                    break;
                case 'PLN':
                    result = amount * plnRateBuy;
                    break;
            }

            results.insertAdjacentHTML('afterbegin', `<p>Monobank will buy your ${amount} ${currency} for ${result} UAH</p>`);
        };

        countSell = (currency, amount) => {
            let result;

            switch (currency) {
                case 'USD':
                    result = amount * usdRateSell;
                    break;
                case 'EUR':
                    result = amount * eurRateSell;
                    break;
                case 'PLN':
                    result = amount * plnRateSell;
                    break;
            }

            results.insertAdjacentHTML('afterbegin', `<p>Monobank will sell you ${amount} ${currency} for ${result} UAH</p>`);
        };
    });
